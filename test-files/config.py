from bookman.config import *

books_json = 'test-files/books.json' #bookman json file
books_dir = 'test-files/' #directory with books
api_key = '' #googlebooks api key
api_key_file = 'test-files/api_key'

extra_attrs = {
    "extra": "default"
}
